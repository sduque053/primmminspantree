#include <iostream>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <iterator>

class graph{

    public:

    graph(int node, int den, int cost);//Initialize an undirected graph with a number of vertex equal to node, edge density den, and max edge cost of cost
    graph(std::ifstream& g); //Initialize graph with a text file of nodes
    graph();        //Initialize an empty undirected graph
    
    void resize(int ns);    //change the number of nodes to ns
    void fill();    //Randomly assign edges with random costs, up to "cost"
    void empty();   //Delete all edges

    std::vector<int> read_node(int n);  //Returns the relation of node with all other nodes
    int read_weight(int n, int m);  //Retruns the value asociated with an edge
    int read_size();    //Returns the number of vertex

    void write_weight(int n, int m, int w); //Sets the value associated with an edge

    friend std::ostream& operator<<(std::ostream& os, const graph& gr);//Operator overloading for printing

    private:

    int size;
    int density;
    int cst;
    std::vector< std::vector<int> > map;


};