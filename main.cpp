#include <iostream>
#include <cstdlib>
#include <vector>
#include <time.h>
#include <fstream>
#include "graph.hpp"


int main(){
    
    int size;

    std::ifstream grp("Tree.txt"); //Initializes Text file

    graph graph1(grp); //Constructor for graph based on text file

    size=graph1.read_size(); //Reads how big the resultin graph is

    graph graphc;   //saves a copy of the graph for later printing
    graphc=graph1;
    
    graph graph2(size,0,0); //empty graph to add the MST edges   
    graph2.empty();

    std::vector<int> nodes(size); //vector with all nodes in graph
    for(int i=0;i<nodes.size();i++) nodes[i]=i;    
    
    std::vector<int> closed; //Nodes currently in the spanning tree 
 
    int start=0;    //starting node
    int min;
    int a_node,a_connection;
    std::vector<int>::iterator addr;

    closed.push_back(nodes[start]); //Adds starting node to spanning tree and removes it from node list
    nodes.erase(nodes.begin()+start);

    std::cout<<"Start"<<"\n";

    while(nodes.size()!=0){
        min=0;
        for(auto IT=closed.begin();IT<closed.end();IT++){ //Iterates through the nodes in the spanning tree
            for(int i=0;i<size;i++){ //Iterates through the edges of the node currently being looked at
                if(graph1.read_weight(*IT,i)>0){ //If the edge exists
                    if(min==0 or min>graph1.read_weight(*IT,i)){ //If its the first iteration or if the current edge is smaller than the one being considered
                        min=graph1.read_weight(*IT,i);//Information about edge being considered
                        a_node=*IT;
                        a_connection=i;
                    } //This all could be replaced with a priority queue

                }

            }
        }
        std::cout<<a_node<<a_connection<<min<<"\n";
        for(int i=0;i<nodes.size();i++){ //Looks for the added node and removes it from the pending list
            if(nodes[i]==a_connection){
                nodes.erase(nodes.begin()+i);
            }
        }
        closed.push_back(a_connection); //Adds node to closed set
        graph2.write_weight(a_node,a_connection,min); //Adds edge to spanning tree
        graph2.write_weight(a_connection,a_node,min);
        graph1.write_weight(a_node,a_connection,0); //Deletes edge from graph
        graph1.write_weight(a_connection,a_node,0);

    }

    //Prints original graph and the resulting spanning tree

    std::cout<<"finished"<<"\n"<<graphc;

    std::cout<<"\nMST\n\n"<<graph2;



}
