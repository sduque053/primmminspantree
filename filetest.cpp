#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include "graph.hpp"

int main(){
    
    std::ifstream g("Nodes.txt");

    graph tree(g);

    std::cout<<tree;

    return 0;
}